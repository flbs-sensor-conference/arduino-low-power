### Instructions for installing and running the code

1. Install git https://git-scm.com/downloads

2. Install Arduino IDE https://www.arduino.cc/en/Main/Software

3. Install Sodaq board manager files

    Open up the Arduino IDE. Go to File->Preferences and enter the following URL in the **Additional Board Manager URLs** text box: `http://downloads.sodaq.net/package_sodaq_samd_index.json`. Then go to Tools->Board->Boards Manager and search for "Sodaq". Find **Sodaq SAMD Boards** and install the latest version (probably 1.6.19).

4. Select the board you will be building the code for

    Go to Tools->Board and select **Sodaq ExpLoRer**

------------

1. Clone this project onto your desktop by right clicking on your desktop and clicking **Git Bash Here**. A terminal window will appear -- enter the command below in the terminal window.

    `git clone https://gitlab.com/flbs-sensor-conference/arduino-low-power.git`

2. Open the Arduino IDE and then open the file **arduino-low-power.ino** located in the project folder that you just cloned.

3. Copy the three libraries contained in the project folder (ArduinoLowPower, RTCZero, arduino-device-lib) into your Arduino libraries folder. This folder is located in C:\Users\YOUR USERNAME\Documents\Arduino\libraries.

------------

Now you are ready to build the code and load it onto a Sodaq ExpLoRer board. In the Arduino IDE, click Sketch->Verify/Compile to build the code. 

If all goes well, connect the board to your PC. Your computer should install any necessary drivers. Click Tools->Ports in the Arduino IDE and select the Sodaq Board -- it might appear as an Arduino Zero or a Sodaq ExpLoRer. Now click Sketch->Upload to upload the code. 