/*****************************************************************************
   Includes
 ****************************************************************************/

#include <TheThingsNetwork.h>
#include <ArduinoLowPower.h>

/*****************************************************************************
   Defines / Global Variables
 ****************************************************************************/

/* Serial debug settings */
#define debugSerial SerialUSB
#define DEBUG_SERIAL_BAUD 57600

/* LoraWAN settings */
const char *appEui = "70B3D57ED001B3DF";                  // MODIFY THIS
const char *appKey = "D6FD85D252CEF49616F889187BE8DD44";  // MODIFY THIS
#define loraSerial Serial2
#define freqPlan TTN_FP_US915
#define LORA_SERIAL_BAUD 57600
#define LORA_RESET_PIN LORA_RESET
TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

/* Sleep mode settings */
#define SLEEP_DURATION_MINUTES 1

/*****************************************************************************
   System State
 ****************************************************************************/

typedef struct {
  float board_temperature;
  float battery_voltage;
} measurements;

measurements measurements_state;

/*****************************************************************************
   Functions
 ****************************************************************************/

void exitDeepSleepMode(void)
{
  ttn.wake();
}

void enterDeepSleepMode(void)
{
  // Sleep for largest amount of time possible
  ttn.sleep(259200000);

  LowPower.deepSleep(60*1000*SLEEP_DURATION_MINUTES);
}

float getBatteryVoltage()
{
  //float mVolts = (float)analogRead(VBAT_MEASURE) * 3300.0 / 1023.0;
  return 123.5;
}

float getTemperature()
{
  float mVolts = (float)analogRead(TEMP_SENSOR) * 3300.0 / 1023.0;
  float temp = (mVolts - 500.0) / 10.0; // 10mV per C, 0C is 500mV

  return temp;
}

/*****************************************************************************
   Init
 ****************************************************************************/

void setup()
{
  /* Init serial ports */
  debugSerial.begin(DEBUG_SERIAL_BAUD);
  loraSerial.begin(LORA_SERIAL_BAUD);
  delay(10000);

  // Turn off bluetooth modem
  pinMode(BLUETOOTH_WAKE, OUTPUT);
  digitalWrite(BLUETOOTH_WAKE, HIGH);

  /* Connect to TTN */
  ttn.showStatus();
  debugSerial.println("\n--- Attempting to join The Things Network ---");
  ttn.join(appEui, appKey);
}

/*****************************************************************************
   Main Loop
 ****************************************************************************/

void loop()
{
  /* Take measurements and send them to TTN */
  measurements_state.board_temperature = getTemperature();
  measurements_state.battery_voltage = getBatteryVoltage();
  ttn.sendBytes((uint8_t*)&measurements_state, sizeof(measurements_state));

  /* Go to sleep */
  enterDeepSleepMode();

  /* Wakeup */
  exitDeepSleepMode();
}
